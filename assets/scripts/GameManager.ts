import { _decorator, CCInteger, Component, instantiate, Label, Node, Prefab, Vec3 } from 'cc';
import { BLOCK_SIZE, PlayerController } from './PlayerController';
const { ccclass, property } = _decorator;

enum GameState {
    INIT,
    PLAYING,
    OVER
}

// TypeScript 枚举中，如果没有给枚举赋值，那么枚举的值会顺序的从 0 开始分配
enum BlockType {
    NONE,
    STONE
};

@ccclass('GameManager')
export class GameManager extends Component {

    // 属性参数只有 type 时也可写作 @property(Prefab)
    // Prefab 预制体
    @property({type: Prefab})
    public boxPrefab: Prefab;

    // 变量
    @property({type: CCInteger})
    public roadLength: number;

    // 控制器
    // ？？？这里类型是 PlayerController，但在属性检查器里配置的是 PlayerController 所挂载的 Player 节点
    @property(PlayerController)
    public playerControler: PlayerController;

    // UI
    @property(Node)
    public startMenu: Node;

    @property(Label)
    public stepsLabel: Label;

    private _road: BlockType[] = [];
    private _stepsCount: number = 0;

    start() {
        this.enterGameState(GameState.INIT);
        // 某个节点派发的事件，只能用这个节点的引用去监听
        this.playerControler.node.on('JumpEnd', this.onPlayerJumpEnd, this);
    }

    update(deltaTime: number): void {
        
    }

    enterGameState(state: GameState) {
        switch(state) {
            case GameState.INIT:
                this.initGame();
                break;
            case GameState.PLAYING:
                this.playGame();
                break;
            case GameState.OVER:
                break;
        }
    }

    // 初始化游戏，初始化地图、角色回到原点且不响应输入、显示游戏 UI、steps 归 0
    initGame() {
        this.generateRoad();

        if (this.playerControler) {
            // playerController 挂载在 player 节点上，设置 player 的 position
            this.playerControler.node.setPosition(Vec3.ZERO);
            this.playerControler.setInputActive(false);
        }

        if (this.startMenu) {
            this.startMenu.active = true;
        }

        this._stepsCount = 0;
        if (this.stepsLabel) {
            this.stepsLabel.string = '0';
        }
    }

    // 进入游戏，隐藏游戏 UI、开启输入响应
    playGame() {
        console.log('Playing...');

        if (this.startMenu) {
            this.startMenu.active = false;
        }

        // 直接设置 active 会直接开始监听鼠标事件（点 UI 按钮时 player 就开始移动），做一下延迟处理
        setTimeout(() => {  
            if (this.playerControler) {
                this.playerControler.setInputActive(true);
            }
        }, 0.1);
    }

    // 生成地图
    generateRoad() {
        // this 为该脚本组件，.node 为组件所挂载在的节点
        // 清除当前脚本挂载节点的所有子节点
        this.node.removeAllChildren();
        this._road = [];
        this._road.push(BlockType.STONE);

        // 计算随机数组
        for (let i = 1; i < this.roadLength; i++) {
            if (this._road[i - 1] === BlockType.NONE) {
                this._road.push(BlockType.STONE);
            } else {
                this._road.push(Math.floor(Math.random() * 2));
            }
        }

        // 绘制方块
        for (let j = 0; j < this.roadLength; j++) {
            // | 代表联合类型，block 类型可以是 Node，也可以是 null
            let block: Node | null = this.spawnBlockByType(this._road[j]);
            if (block) {
                this.node.addChild(block);
                block.setPosition(j * BLOCK_SIZE, 0, 0);
            }
        }
    }

    spawnBlockByType(blockType: BlockType): Node | null {
        if (!this.boxPrefab) {
            console.error("boxPrefab not defined!");
            return null;
        }

        let block: Node;
        switch(blockType) {
            case BlockType.STONE:
                // 从 Prefab 实例化出新节点
                block = instantiate(this.boxPrefab);
                break;
        }

        return block;
    }

    onStartButtonClicked() {
        this.enterGameState(GameState.PLAYING);
    }

    onPlayerJumpEnd(step: number) {
        this._stepsCount += step;
        if (this.stepsLabel) {
            // number 转 string
            this.stepsLabel.string = '' + this._stepsCount;
        }

        if (this._stepsCount >= this.roadLength) {
            this.enterGameState(GameState.INIT);
            return;
        }
        if (this._road[this._stepsCount] === BlockType.NONE) {
            this.enterGameState(GameState.INIT);
        }
    }
}

