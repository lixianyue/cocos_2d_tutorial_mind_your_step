// TypeScript 内置库里也有 Node 和 Animation，这里要写明 import from 'cc'
import { _decorator, Component, EventMouse, Input, input, Node, Vec3, Animation, EventTouch } from 'cc';
const { ccclass, property } = _decorator;

// 加了 export 关键字，可以在该脚本以外访问
export const BLOCK_SIZE = 40;

@ccclass('PlayerController')
export class PlayerController extends Component {

    // 下划线_开头为 private 属性
    private _startJump: boolean = false;
    private _jumpStep: number = 0;
    private _curJumpTime: number = 0;
    private _jumpTime: number;
    private _curJumpSpeed: number = 0;
    // pos 必须用向量表示
    private _curPos: Vec3 = new Vec3();
    private _deltaPos: Vec3 = new Vec3(0, 0, 0);
    private _targetPos: Vec3 = new Vec3();

    // 在 inspector 属性检查器里可配置的属性
    @property(Animation)
    BodyAnim: Animation;

    @property(Node)
    leftTouch: Node

    @property(Node)
    rightTouch: Node


    start() {
        
    }

    // 每帧调用一次，渲染当前帧画面
    update(deltaTime: number) {
        // 跳跃中，进行处理
        if (this._startJump) {
            this._curJumpTime += deltaTime;
            if (this._curJumpTime > this._jumpTime) {
                // 渲染跳跃结束态
                // 注意：position 是相对父节点的 position，不是全局 position
                this.node.setPosition(this._targetPos);
                this._startJump = false;
                // 发出跳跃结束信号，并发送步数
                this.node.emit('JumpEnd', this._jumpStep);
            } else {
                // 渲染跳跃中间态
                this.node.getPosition(this._curPos);
                // 横向移动，更新 x 坐标
                this._deltaPos.x = this._curJumpSpeed * deltaTime;
                // Vec3 的静态方法，第 2、3 个参数相加之后赋给第 1 个参数
                Vec3.add(this._curPos, this._curPos, this._deltaPos);
                this.node.setPosition(this._curPos);
            }
        }
    }

    setInputActive(active: boolean) {
        if (active) {
            // 注册鼠标输入监听回调方法，input.on() 会监听所有输入
            // input.on(Input.EventType.MOUSE_UP, this.onMouseUp, this);

            // 注册左右触摸区域监听回调方法，node.on() 只会监听 node 范围内的输入
            // Input.EventType.TOUCH_START 既包含触摸事件，也包含鼠标事件，所以不需要 MOUSE_UP 了
            this.leftTouch.on(Input.EventType.TOUCH_START, this.onTouchStart, this);
            this.rightTouch.on(Input.EventType.TOUCH_START, this.onTouchStart, this);
        } else {
            // 取消鼠标输入监听回调方法
            // input.off(Input.EventType.MOUSE_UP, this.onMouseUp, this);

            // 取消左右触摸区域监听回调方法
            this.leftTouch.off(Input.EventType.TOUCH_START, this.onTouchStart, this);
            this.rightTouch.off(Input.EventType.TOUCH_START, this.onTouchStart, this);
        }
    }

    // 鼠标点击一下，触发一次跳跃
    onMouseUp(event: EventMouse) {
        // 左键 0，右键 2
        if (event.getButton() === 0) {
            this.jumpByStep(1);
        } else if (event.getButton() === 2) {
            this.jumpByStep(2);
        }
    }

    onTouchStart(event: EventTouch) {
        const target = event.target as Node;
        if (target.name === 'LeftTouch') {
            this.jumpByStep(1);
        } else if (target.name === 'RightTouch') {
            this.jumpByStep(2);
        }
    }

    // 如有必要（跳跃未结束不二次跳跃），初始化或更新跳跃相关的数据
    jumpByStep(step: number) {
        if (this._startJump) {
            return;
        }
        this._startJump = true;
        this._curJumpTime = 0;
        this._jumpStep = step;

        const animationClip = step === 1 ? 'oneStep' : 'twoStep';
        this._jumpTime = this.BodyAnim.getState(animationClip).duration;

        this._curJumpSpeed = this._jumpStep * BLOCK_SIZE / this._jumpTime;
        // 获取到的 position 再赋给 this._curPos，一种可复用 Vec3 的写法
        this.node.getPosition(this._curPos);
        // 计算 targetPosition
        // Vec3 的静态方法 add，把第 2、3 个参数加起来，赋给第 1 个参数
        Vec3.add(this._targetPos, this._curPos, new Vec3(this._jumpStep * BLOCK_SIZE, 0, 0));
        // 可以把下面这行注释掉看看效果 :) 注意：向量是引用，不是值，所以要用 .clone()
        // this._targetPos = this._curPos.clone();

        // 判 null
        if (this.BodyAnim) {
            if (step === 1) {
                this.BodyAnim.play('oneStep');
            } else if (step === 2) {
                this.BodyAnim.play('twoStep');
            }
        }
    }
}

